# rusty-site-inspector

This project aims to become a CLI application. This application will be able to obtain certain information concerning a website. Stacks to use, networking information, and other features.

# Usage
### Run
```cargo run -- -u <URL>```

### Build 
```cargo build```

### Command
```cargo run front_spec --url <URL>```
```cargo run back_spec --url <URL>```

## Road Map for v1.0
### Front End stack analyzer:
- [x] Analizing librairies JavaScript use (Jquery, Bootsrap, React, ect).
- [x] Detecting CSS Framework (Bootstrap, Tailwind, Fundation, ect).
- [x] Identification of dependency managers (npm, Yarn).
### Back End analyzer: 
- [x] Discovery web server (Apache, Nginx, Tomcat, ect).
- [x] Identification of back end language (Node.js, Deno, Ruby, PHP).
- [x] Detecting framework used (Express.js, Django, SF, Larabel, ect).
### Analyzing of the security:
- [ ] Verify cert SSL/TLS.
- [ ] Analizing header security HTTP (CSP, HSTS, ect).
- [ ] Wanted known vulnerability (CVE).
### Performance and Optimization:
- [ ] Evaluate speed loading.
- [ ] Metrics of performance front-end (Time of preview, ressources sizing, ect).
- [ ] Tips of optimization (Compressed images, min css/js, ect).
### Send/Export Rapport: 
- [ ] Generate detailed rapport in JSON or HTML.
### External Tools Integration: 
- [ ] Implement Google Lighthouse. 
- [ ] ExploitDB for detailed rapport. 

### Todo In Progress
- [x] Transformation into a CLI application.
- [ ] Check for duplicate code function.
- [ ] Change structure of `data_dep_managers()` and `data_stack()`
- [x] Make a pipelines.
- [x] Add a system proxy to reqwest client (no utils). 
- [x] Extract css/js script.
- [x] Extract http header informations.
