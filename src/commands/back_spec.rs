use clap:: {Arg, ArgMatches, Command};

use crate::backend_extractor::back_stacks::get_http_header_bypass_cloudflare;

pub fn command() -> Command<'static> {
    Command::new("back_spec")
        .about("This command return stacks of back-end side.")
        .arg(
            Arg::new("url")
                .short('u')
                .long("url")
                .value_name("URL")
                .help("Website URL")
                .takes_value(true)
                .required(true),
        )
        .arg(
            Arg::new("report")
                .short('r')
                .long("report")
                .help("Make a report file")
                .required(false),
        )
}

pub fn handle(matches: &ArgMatches) {
    let url = matches.value_of("url").unwrap();
    let report = matches.is_present("report");

    let result = get_http_header_bypass_cloudflare(url);

    match result {
        Ok(_) => {
            // Remove the println! statement or use a different way to display the value.
            if report {
                println!("Report file created");
            }
        }
        Err(e) => {
            eprintln!("Error: {:?}", e);
        }
    }
}