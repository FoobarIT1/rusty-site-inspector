use clap:: {Arg, ArgMatches, Command};
use reqwest::blocking::get;

use crate::frontend_extractor::data_stack::{FrontendStack, get_frontend_stacks};
use crate::frontend_extractor::data_dep_managers::{DependencyManager, get_dependency_managers};
use crate::frontend_extractor::read_script::get_stack_on_script;
use scraper::{Html, Selector};
use std::error::Error;


pub fn command() -> Command<'static> {
    Command::new("front_spec")
        .about("This command return stacks of front-end side.")
        .arg(
            Arg::new("url")
                .short('u')
                .long("url")
                .value_name("URL")
                .help("Website URL")
                .takes_value(true)
                .required(true),
        )
        .arg(
            Arg::new("report")
                .short('r')
                .long("report")
                .help("Make a report file")
                .required(false),
        )
}

pub fn handle(matches: &ArgMatches) {
    let url = matches.value_of("url").unwrap();
    let reports = matches.is_present("report");

    println!("Front-end stack used: {}", url);

    if reports {
        println!("JSON file generated or follow this url for HTML views: http://localhost:1337/report");
    }
    
    match inspect_front(url) {
        Ok(stacks) => {
            if stacks.is_empty() {
                println!("No front-end stack find.");
            } else {
                println!("Front-end stacks found:");
                for stack in stacks {
                    println!("- {} (Typefile: {})", stack.name, stack.typefile);
                }
            }
        } 
        Err(err) => {
            eprintln!("Error inspect site: {}", err);
        }
    }
    match inspect_dependency_managers(url) {
        Ok(managers) => {
            if managers.is_empty() {
                println!("No dependency manage find.");
            } else {
                println!("Dependency's Manager:");
                for manager in managers {
                    println!("- {}", manager.name);
                }
            }
        }
        Err(_err) => {
            eprint!("Error inspecting dependency's")
        }
    }

    match get_stack_on_script(url) {
        Ok(()) => {
            
        }
        Err(_err) => {
            eprint!("Error from get_stack_on_script()")
        }
    }
       
    fn inspect_dependency_managers(url: &str) -> Result<Vec<DependencyManager>, Box<dyn Error>> {
        let response = reqwest::blocking::get(url)?.text()?;
        let document = scraper::Html::parse_document(&response);
        let mut detected_managers = Vec::new();
    
        let dependency_managers = get_dependency_managers();
    
        for manager in dependency_managers {
            if document.root_element().html().contains(manager.indicator) {
                detected_managers.push(manager);
            } 
        }
    
        Ok(detected_managers)
    }
   
    fn inspect_front(url: &str) -> Result<Vec<FrontendStack>, Box<dyn Error>> {
        let response = get(url)?.text()?;
      
        let document = Html::parse_document(&response);
        let mut detected_stacks = Vec::new();
    
        let frontend_stacks = get_frontend_stacks();
    
        for stack in frontend_stacks {
            let selector = Selector::parse(&stack.identifier).unwrap();
            if document.select(&selector).next().is_some() {
                detected_stacks.push(stack);
            }
        }
    
        Ok(detected_stacks)
    }
} 

