use std::error::Error;
use reqwest::blocking::Client;
use url::Url;

pub fn get_http_header_bypass_cloudflare(url: &str) -> Result<(), Box<dyn Error>> {
    let client = Client::builder()
        .build()?;

    let url = Url::parse(url)?;
    let response = client
        .get(url.clone())
        .header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36")
        .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8")
        .header("Accept-Language", "en-US,en;q=0.5")
        .header("Connection", "keep-alive")
        .send()?;

    if let Some(server) = response.headers().get("server") {
        println!("Server: {:?}", server.to_str()?);
    } else {
        println!("Server header not found");
    }

    if let Some(x_powered_by) = response.headers().get("x-powered-by") {
        println!("X-Powered-By: {:?}", x_powered_by.to_str()?);
    } else {
        println!("X-Powered-By: header not found");
    }
    Ok(())
}

#[test]
fn test_get_http_header_bypass_cloudflare() {
    let url = "https://www.example.com";
    let result = get_http_header_bypass_cloudflare(url);
    assert!(result.is_ok());
}