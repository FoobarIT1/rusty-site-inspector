mod commands;
mod frontend_extractor;
mod backend_extractor;

use clap::builder::Command;
//use std::error::Error;

fn main() {
    let matches = Command::new("rusty-site-inspector")
        .version("1.0")
        .author("https://gitlab.com/FoobarIT1")
        .about("Site inspector for web developer and audit")
        .subcommand(commands::front_spec::command())
        .subcommand(commands::back_spec::command())
        .get_matches();

    match matches.subcommand() {
       Some(("front_spec", sub_matches)) => commands::front_spec::handle(sub_matches),
       Some(("back_spec", sub_matches)) => commands::back_spec::handle(sub_matches),
       _ => println!("Command not found"),

    }    
}