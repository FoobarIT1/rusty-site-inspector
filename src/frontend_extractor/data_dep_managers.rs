pub struct DependencyManager {
    pub name: &'static str,
    pub indicator: &'static str,
}

pub fn get_dependency_managers() -> Vec<DependencyManager> {
    vec![
        DependencyManager { name: "npm", indicator: "node_modules" },
        DependencyManager { name: "yarn", indicator: "yarn.lock" },
        DependencyManager { name: "bower", indicator: "bower_components" },
    ]
}

