pub struct FrontendStack {
    pub name: &'static str,
    pub identifier: &'static str,
    pub typefile: &'static str,
}

pub fn get_frontend_stacks() -> Vec<FrontendStack> {
    let stacks = vec![
        FrontendStack {
            name: "React",
            identifier: "script[src*='react']",
            typefile: "js",
        },
        FrontendStack {
            name: "Vue.js",
            identifier: "script[src*='vue']",
            typefile: "js",
        },
        FrontendStack {
            name: "Angular",
            identifier: "script[src*='angular']",
            typefile: "js",
        },
        FrontendStack {
            name: "Bootstrap",
            identifier: "script[src*='bootstrap']",
            typefile: "js"
        },
        FrontendStack {
            name: "Preact",
            identifier: "script[src*='preact']",
            typefile: "js"
        },
        FrontendStack {
            name: "CoreJS",
            identifier: "script[src*='core-js']",
            typefile: "js"
        },
        FrontendStack {
            name: "Jquery",
            identifier: "script[src*='jquery']",
            typefile: "js"
        },
        FrontendStack {
            name: "Bootstrap",
            identifier: "link[href*='bootstrap']",
            typefile: "css"
        },
        FrontendStack {
            name: "Tailwind CSS",
            identifier: "link[href*='tailwind']",
            typefile: "css"
        },
    ];

    stacks
}