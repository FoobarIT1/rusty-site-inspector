use reqwest::blocking::get;
use url::Url;
use scraper::{Html, Selector};
use std::error::Error;
use headless_chrome::{Browser, LaunchOptions};

fn fetch_page(url: &str) -> Result<String, Box<dyn Error>> {
    let response = get(url)?;
    let body = response.text()?;
    Ok(body)
}

fn parse_html(html: &str) -> Html {
    Html::parse_document(html)
}

fn detect_frameworks(html: &Html) -> Vec<String> {
    let mut frameworks = Vec::new();

    if html.root_element().html().contains("react") || html.root_element().html().contains("ReactDOM") {
        frameworks.push("React".to_string());
    }

    let ng_selector = Selector::parse("[ng-app], [ng-controller]").unwrap();
    if html.select(&ng_selector).next().is_some() {
        frameworks.push("Angular".to_string());
    }

    if html.root_element().html().contains("vue") || html.root_element().html().contains("Vue") {
        frameworks.push("Vue.js".to_string());
    }

    frameworks
}

fn fetch_script_urls(html: &Html, base_url: &str) -> Vec<String> {
    let mut script_urls = Vec::new();
    let script_selector = Selector::parse("script[src]").unwrap();
    let base = Url::parse(base_url).expect("Invalid base URL");

    for element in html.select(&script_selector) {
        if let Some(src) = element.value().attr("src") {
            let script_url = Url::parse(src);
            match script_url {
                Ok(url) => {
                    // Check if the URL is from the same domain as the base URL
                    if url.domain() == base.domain() {
                        script_urls.push(url.to_string());
                    }
                },
                Err(_) => {
                    // If URL parsing failed, assume it is a relative URL
                    let relative_url = base.join(src).expect("Invalid relative URL");
                    script_urls.push(relative_url.to_string());
                },
            }
        }
    }

    println!("Script found {:?}", script_urls);
    script_urls
}

fn detect_frameworks_in_scripts(script_urls: &[String]) -> Result<Vec<String>, Box<dyn Error>> {
    let mut frameworks = Vec::new();

    for url in script_urls {
        let response = get(url)?;
        let script_content = response.text()?;
        if script_content.contains("React") || script_content.contains("ReactDOM") {
            frameworks.push("React".to_string());
        }
        if script_content.contains("Angular") {
            frameworks.push("Angular".to_string());
        }
        if script_content.contains("Vue") {
            frameworks.push("Vue.js".to_string());
        }
    }

    Ok(frameworks)
}

fn detect_frameworks_with_headless_browser(url: &str) -> Result<Vec<String>, Box<dyn Error>> {
    let mut frameworks = Vec::new();

    let browser = Browser::new(LaunchOptions {
        headless: true,
        ..Default::default()
    })?;

    let tab = browser.new_tab()?;
    tab.navigate_to(url)?.wait_until_navigated()?;

    let has_react = tab.evaluate("window.React || window.ReactDOM", true)?.value.is_some();
    if has_react {
        frameworks.push("React".to_string());
    }

    let has_angular = tab.evaluate("window.angular || document.querySelector('[ng-app], [ng-controller]')", true)?.value.is_some();
    if has_angular {
        frameworks.push("Angular".to_string());
    }

    let has_vue = tab.evaluate("window.Vue", true)?.value.is_some();
    if has_vue {
        frameworks.push("Vue.js".to_string());
    }

    Ok(frameworks)
}

pub fn get_stack_on_script(url: &str) -> Result<(), Box<dyn Error>> {
    let html_content = fetch_page(url)?;
    let html = parse_html(&html_content);
    let mut frameworks = detect_frameworks(&html);

    let script_urls = fetch_script_urls(&html, &url);
    let script_frameworks = detect_frameworks_in_scripts(&script_urls)?;

    frameworks.extend(script_frameworks);

    let browser_frameworks = detect_frameworks_with_headless_browser(url)?;
    frameworks.extend(browser_frameworks);

    for framework in frameworks {
        println!("Detected framework: {}", framework);
    }

    Ok(())
}
